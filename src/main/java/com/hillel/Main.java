package com.hillel;


import com.hillel.db.connection.DataSourceFactory;
import com.hillel.db.dao.impl.ManufacturerDao;
import com.hillel.db.dao.impl.VehicleDao;
import com.hillel.db.model.Manufacturer;
import com.hillel.db.model.Vehicle;
import com.hillel.service.ManufacturerService;
import com.hillel.service.VehicleService;
import org.h2.tools.RunScript;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws SQLException, FileNotFoundException {
        DataSource H2DataSource = DataSourceFactory.H2.configuration.configure();

        RunScript.execute(H2DataSource.getConnection(),
                new FileReader("src/main/resources/init_database.sql"));
        //init database for testing purposes

        ManufacturerDao manufacturerDao = new ManufacturerDao(H2DataSource);
        VehicleDao vehicleDao = new VehicleDao(H2DataSource);

        VehicleService vehicleService = new VehicleService(vehicleDao);

        ManufacturerService manufacturerService = new ManufacturerService(manufacturerDao, vehicleDao);

        Manufacturer manufacturer = Manufacturer.builder().name("BMW").build();

        Vehicle vehicle1 = Vehicle.builder().color("white").seats(2).wheels(4).build();
        Vehicle vehicle2 = Vehicle.builder().color("red").seats(4).wheels(4).build();
        Vehicle vehicle3 = Vehicle.builder().color("green").seats(4).wheels(4).build();
        Vehicle vehicle4 = Vehicle.builder().color("blue").seats(2).wheels(2).build();

        List<Vehicle> vehiclesToCreate = List.of(vehicle1, vehicle2, vehicle3, vehicle4);

        manufacturerService.createManufacturer(manufacturer, vehiclesToCreate);

        List<Manufacturer> allManufacturers = manufacturerService.getAll();
        System.out.println(allManufacturers);

        List<Vehicle> vehicleList = vehicleService.getAll();
        System.out.println(vehicleList);
    }
}

