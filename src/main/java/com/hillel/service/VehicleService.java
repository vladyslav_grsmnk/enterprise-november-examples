package com.hillel.service;

import com.hillel.db.dao.impl.VehicleDao;
import com.hillel.db.model.Manufacturer;
import com.hillel.db.model.Vehicle;

import java.util.List;
import java.util.stream.Collectors;

public class VehicleService {
    private final VehicleDao vehicleDao;

    public VehicleService(VehicleDao vehicleDao) {
        this.vehicleDao = vehicleDao;
    }

    public Vehicle createVehicle(Vehicle vehicle) {
        return vehicleDao.create(vehicle);
    }

    public void createVehicles(List<Vehicle> vehicles) {
        vehicleDao.create(vehicles);
    }

    public Vehicle getVehicleById(long id) {
        return vehicleDao.get(id);
    }

    public List<Vehicle> getAll() {
        return vehicleDao.getAll();
    }

    public boolean update(Vehicle vehicle) {
        return vehicleDao.update(vehicle);
    }

    public boolean delete(Vehicle vehicle) {
        return vehicleDao.update(vehicle);
    }

    public List<Vehicle> getAllVehiclesByManufacturer(Manufacturer manufacturer) {
        return vehicleDao
                .getAll()
                .stream()
                .filter(v -> v.getManufacturerId()
                        .equals(manufacturer.getId()))
                .collect(Collectors.toList());

    }
}
