package com.hillel.db.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Vehicle {
    private long vehicleId;
    private String color;
    private int wheels;
    private int seats;
    private Long manufacturerId;
}
