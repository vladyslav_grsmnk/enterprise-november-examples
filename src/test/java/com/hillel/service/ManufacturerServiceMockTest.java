package com.hillel.service;

import com.hillel.db.dao.impl.ManufacturerDao;
import com.hillel.db.dao.impl.VehicleDao;
import com.hillel.db.model.Manufacturer;
import com.hillel.db.model.Vehicle;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ManufacturerServiceMockTest {

    private static final VehicleDao vehicleDaoMock = mock(VehicleDao.class);
    private static final ManufacturerDao manufacturerDaoMock = mock(ManufacturerDao.class);

    private static final long TEST_ID_MANUFACTURER = 1L;
    private static final Manufacturer TEST_MANUFACTURER_EXPECTED = new Manufacturer(TEST_ID_MANUFACTURER, "BMW");

    private ManufacturerService manufacturerService =
            new ManufacturerService(manufacturerDaoMock, vehicleDaoMock);

    @Test
    void whenCreateManufacturerWithVehiclesShouldCreateOneTest() {
        //given

        when(manufacturerDaoMock.create(buildTestManufacturer()))
                .thenReturn(TEST_MANUFACTURER_EXPECTED);

        //when

        Vehicle vehicle1 = Mockito.spy(Vehicle.builder().color("Red").seats(4).wheels(4).build());
        Vehicle vehicle2 = Mockito.spy(Vehicle.builder().color("White").seats(2).wheels(2).build());

        Manufacturer actual = manufacturerService.createManufacturer(buildTestManufacturer(),
                List.of(vehicle1, vehicle2));

        //then
        verify(vehicleDaoMock).create(List.of(vehicle1, vehicle2));

        verify(vehicle1).setManufacturerId(TEST_ID_MANUFACTURER);
        verify(vehicle2).setManufacturerId(TEST_ID_MANUFACTURER);

        assertEquals(TEST_MANUFACTURER_EXPECTED, actual);
    }

    Manufacturer buildTestManufacturer() {
        return new Manufacturer(TEST_ID_MANUFACTURER, "BMW");
    }

}
