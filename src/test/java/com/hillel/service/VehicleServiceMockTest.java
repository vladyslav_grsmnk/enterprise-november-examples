package com.hillel.service;

import com.hillel.db.dao.impl.VehicleDao;
import com.hillel.db.model.Manufacturer;
import com.hillel.db.model.Vehicle;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VehicleServiceMockTest {

    private static final VehicleDao vehicleDaoMock = mock(VehicleDao.class);

    private static final long TEST_ID = 1L;
    private static final Vehicle TEST_VEHICLE_EXPECTED = Vehicle.builder()
            .vehicleId(TEST_ID).color("Red").seats(4).wheels(4).build();

    private VehicleService vehicleService = new VehicleService(vehicleDaoMock);

    @Test
    void whenCreateVehicleShouldCreateOneTest() {
        //given

        when(vehicleDaoMock.create(buildTestVehicle()))
                .thenReturn(TEST_VEHICLE_EXPECTED);

        //when
        Vehicle actual = vehicleService.createVehicle(buildTestVehicle());

        //then
        assertEquals(TEST_VEHICLE_EXPECTED, actual);
    }

    @Test
    void whenGetAllVehiclesByManufacturerShouldReturnRightVehicleTest() {
        List<Vehicle> allVehicles = List.of(
                Vehicle.builder().color("Red").seats(4).wheels(4).manufacturerId(1l).build(),
                Vehicle.builder().color("Red").seats(4).wheels(4).manufacturerId(2l).build());

        when(vehicleDaoMock.getAll()).thenReturn(allVehicles);

        Manufacturer actual = Manufacturer.builder().name("BMW").id(2l).build();

        List<Vehicle> actualVehicles = vehicleService.getAllVehiclesByManufacturer(actual);

        assertEquals(1, actualVehicles.size());
        assertTrue(actualVehicles.contains(Vehicle.builder().color("Red").seats(4).wheels(4).manufacturerId(2l).build()));

    }

    private Vehicle buildTestVehicle() {
        return Vehicle.builder().color("Red").seats(4).wheels(4).build();
    }

}
