package pointcut;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@ComponentScan("pointcut")
@EnableAspectJAutoProxy
public class Main {
    public static void main(String[] args) {

        ApplicationContext ctx = new AnnotationConfigApplicationContext(Main.class);
        EmployeeService employeeService = ctx.getBean(EmployeeService.class);
        Employee employee = employeeService.getEmployee();
        System.out.println(employee.getName());
        employee.throwException();
    }
}

