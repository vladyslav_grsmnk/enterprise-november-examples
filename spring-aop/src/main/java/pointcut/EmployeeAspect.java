package pointcut;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class EmployeeAspect {

    @Before("execution(public String getName())")
    public void getNameAdvice() {

        System.out.println("Executing Advice on getName()");
    }

    @Before("execution(* pointcut.*.get*())")
    public void getAllAdvice() {
        System.out.println("Service method getter called");
    }

    @Around("execution(* pointcut.Employee.getName())")
    public Object employeeAroundAdvice(ProceedingJoinPoint proceedingJoinPoint) {
        System.out.println("Before invoking getName() method");
        Object value = null;
        try {
            value = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        System.out.println("After invoking getName() method. Return value=" + value);
        return value;
    }

    @Before("@annotation(pointcut.Loggable)")
    public void myAdvice() {
        System.out.println("Executing myAdvice!!");
    }
}
