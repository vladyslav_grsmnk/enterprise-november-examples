package pointcut;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Employee {

    private String name;

    public String getName() {
        return name;
    }

    @Loggable
    @Value("Volodymyr")
    public void setName(String nm) {
        this.name = nm;
    }

    public void throwException() {
        throw new RuntimeException("Dummy Exception");
    }
}
