package qualifier;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("qualifier")
public class StudentContext {

    @Bean
    public Student studentIvan() {
        return new Student("Ivan");
    }

    @Bean
    public Student studentPetro() {
        return new Student("Petro");
    }
}
