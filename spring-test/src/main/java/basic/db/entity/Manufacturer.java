package basic.db.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Manufacturer {

    private Long id;
    private String name;

    public Manufacturer(long id, Manufacturer manufacturer) {
        this.id = id;
        this.name = manufacturer.getName();
    }
}
