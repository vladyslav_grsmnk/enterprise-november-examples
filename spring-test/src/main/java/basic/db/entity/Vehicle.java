package basic.db.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Vehicle {

    private Long vehicleId;
    private String color;
    private int wheels;
    private int seats;
    private Long manufacturerId;

    public Vehicle(String color, int wheels, int seats, Long manufacturerId) {
        this.color = color;
        this.wheels = wheels;
        this.seats = seats;
        this.manufacturerId = manufacturerId;
    }

    public Vehicle(String color, int wheels, int seats) {
        this.color = color;
        this.wheels = wheels;
        this.seats = seats;
    }

    public Vehicle(long vehicleId, Vehicle vehicle) {
        this.vehicleId = vehicleId;
        this.color = vehicle.getColor();
        this.wheels = vehicle.getWheels();
        this.seats = vehicle.getSeats();
        this.manufacturerId = vehicle.getManufacturerId();

    }

    public Vehicle(long id, String color, int wheels, int seats) {
        this.vehicleId = id;
        this.color = color;
        this.wheels = wheels;
        this.seats = seats;
    }
}
