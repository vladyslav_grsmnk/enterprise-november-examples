package basic.service.impl;

import basic.db.dao.impl.ManufacturerDao;
import basic.db.dao.impl.VehicleDao;
import basic.db.entity.Manufacturer;
import basic.db.entity.Vehicle;
import basic.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {
    private final ManufacturerDao manufacturerDao;
    private final VehicleDao vehicleDao;

    @Autowired
    public ManufacturerServiceImpl(ManufacturerDao manufacturerDao, VehicleDao vehicleDao) {
        this.manufacturerDao = manufacturerDao;
        this.vehicleDao = vehicleDao;
    }

    public Manufacturer createManufacturer(Manufacturer manufacturer) {
        return manufacturerDao.create(manufacturer);
    }

    public Manufacturer createManufacturer(Manufacturer manufacturer, List<Vehicle> vehicles) { //important
        Manufacturer createdManufacturer = manufacturerDao.create(manufacturer);
        for (Vehicle vehicleToCreate : vehicles) {
            vehicleToCreate.setManufacturerId(createdManufacturer.getId());
        }
        vehicleDao.create(vehicles);
        return createdManufacturer;
    }

    public List<Vehicle> getAllVehiclesByManufacturer(Manufacturer manufacturer) {
        //todo
        return null;
    }

    public void createManufacturers(List<Manufacturer> manufacturers) {
        manufacturerDao.create(manufacturers);
    }

    public Manufacturer getManufacturerById(long id) {
        return manufacturerDao.get(id);
    }

    public List<Manufacturer> getAll() {
        return manufacturerDao.getAll();
    }

    public boolean update(Manufacturer manufacturer) {
        return manufacturerDao.update(manufacturer);
    }

    public boolean delete(Manufacturer manufacturer) {
        return manufacturerDao.update(manufacturer);
    }
}
