package basic;

import basic.configuration.ProjectConfiguration;
import basic.db.entity.Vehicle;
import basic.service.VehicleService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {

        ApplicationContext ctx = new AnnotationConfigApplicationContext(ProjectConfiguration.class);
        VehicleService vehicleService = ctx.getBean("vehicleServiceImpl", VehicleService.class);

        vehicleService.createVehicle(Vehicle.builder()
                .color("red")
                .seats(5)
                .wheels(4)
                .manufacturerId(null)
                .build());
    }
}
