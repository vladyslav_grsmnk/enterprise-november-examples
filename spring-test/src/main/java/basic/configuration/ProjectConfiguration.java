package basic.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("basic")
public class ProjectConfiguration {

}
