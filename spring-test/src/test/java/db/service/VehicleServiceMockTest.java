package db.service;

import basic.configuration.ProjectConfiguration;
import basic.db.dao.impl.VehicleDao;
import basic.db.entity.Vehicle;
import basic.service.impl.VehicleServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ProjectConfiguration.class})
public class VehicleServiceMockTest {

    private static final long TEST_ID = 1L;
    private static final Vehicle TEST_VEHICLE_EXPECTED = new Vehicle(TEST_ID, "Red", 4, 4);

    @Autowired
    private VehicleServiceImpl vehicleService;

    @MockBean
    private VehicleDao vehicleDaoMock;

    @Test
    void whenCreateVehicleShouldCreateOne() {
        //given

        when(vehicleDaoMock.create(buildTestVehicle())).thenReturn(TEST_VEHICLE_EXPECTED);

        //when
        Vehicle actual = vehicleService.createVehicle(buildTestVehicle());

        //then
        assertEquals(TEST_VEHICLE_EXPECTED, actual);
    }

    private Vehicle buildTestVehicle() {
        return Vehicle.builder().color("Red").seats(4).wheels(4).build();
    }

}
