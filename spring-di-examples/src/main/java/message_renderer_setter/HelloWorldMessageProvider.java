package message_renderer_setter;

public class HelloWorldMessageProvider implements MessageProvider {

    public HelloWorldMessageProvider() {

        System.out.println(" --> HelloWorldMessageProvider: constructor called");
    }

    @Override
    public String getMessage() {
        return "Hello World!";
    }

    @Override
    public int getAge() {
        return 0;
    }

    public void test() {
        System.out.println("Hello");
    }
}