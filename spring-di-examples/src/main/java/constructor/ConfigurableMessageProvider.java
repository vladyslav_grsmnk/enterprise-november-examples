package constructor;


import message_renderer_setter.MessageProvider;

public class ConfigurableMessageProvider implements MessageProvider {
    private String message = "Default message";
    private int age;

    public ConfigurableMessageProvider() {

    }

    public ConfigurableMessageProvider(String message) {
        this.message = message;
    }

    public ConfigurableMessageProvider(String message, int age) {
        this.message = message;
        this.age = age;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
