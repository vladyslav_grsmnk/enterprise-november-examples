package reference;

import oracle_examples.Oracle;

public class InjectRef {
    private Oracle oracle;

    public void setOracle(Oracle oracle) {
        this.oracle = oracle;
    }

    public String toString() {
        return oracle.defineMeaningOfLife();
    }
}